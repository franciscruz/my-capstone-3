import Doughnut from '../components/DoughnutChart';
import {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';

export default function trend(){

	const [totalIncome, setTotalIncome] = useState(0);
	const [totalExpense, setTotalExpense] = useState(0);

	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}api/users/get-records`,{
            method: 'POST',
            headers: {
                 Authorization: `Bearer ${localStorage.getItem('token')}`
             }
             })
		.then(res => res.json())
		.then(data =>{

			if(data.length){
			let totalin = 0;
			const incomeArray = data.filter(category => {
               return(
                	category.categoryType === 'income'
                	)
              		})
				incomeArray.forEach(category => {
				totalin += category.amount
			})

			let totalex = 0;
			const expenseArray = data.filter(category => {
               return(
                	category.categoryType === 'expense'
                	)
              		})
				expenseArray.forEach(category => {
				totalex += category.amount
			})
		    setTotalIncome(totalin)
		    setTotalExpense(totalex)
			}
		})
	}, [])

	return(
			<React.Fragment>
				<Container>
				<Doughnut income = {totalIncome} expense = {totalExpense}/>
				</Container>
			</React.Fragment>
		)
}