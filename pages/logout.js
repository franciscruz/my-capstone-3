import { useContext, useEffect } from 'react';
import UserContext from '../UserContext';
import Router from 'next/router';

export default function index(){
	const {clearToken} = useContext(UserContext)

	useEffect(() => {
		clearToken();
		Router.push('/login')
	}, [])

	return null
}