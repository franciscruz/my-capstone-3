import { useState, useEffect } from 'react';
import NavBar from '../components/NavBar';
import Head from 'next/head'
import '../styles/globals.css';
import 'bootswatch/dist/solar/bootstrap.min.css';

import { UserProvider } from '../UserContext';

function MyApp({ Component, pageProps }) {

	const [token , setToken] = useState(null);

	useEffect(() => {
		setToken(localStorage.getItem('token'));
	}, [])

	const clearToken = () => {
    localStorage.clear();
    setToken(null);
  }

  return(
  	<React.Fragment>
	  		<Head>
		  		<link rel="icon" href="nxt.png" sizes="16x16" type="image/png" />
		  	</Head>
	  	<UserProvider value={{token, setToken, clearToken}}>
		  	<NavBar/>
		  	<Component {...pageProps} />
	  	</UserProvider>
  	</React.Fragment>
  )
}

export default MyApp
