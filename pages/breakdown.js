import LineChart from '../components/LineChart';

import {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';

export default function breakdown(){

	const [dateArray, setDateArray] = useState([]);
	const [incomeArray, setIncomeArray] = useState([]);
    const [expenseArray, setExpenseArray] = useState([]);

	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}api/users/get-records`,{
            method: 'POST',
            headers: {
                 Authorization: `Bearer ${localStorage.getItem('token')}`
             }
             })
		.then(res => res.json())
		.then(data =>{

			if(data.length){

				const dateArr = data.map(category => {
	               return(
	                	new Date(category.dateAdded).toLocaleString()
	                	)
	            })

				const incomeArr = data.map(category => {
	              if(category.categoryType === 'income'){
	              	return(
	              		category.amount 
	              		)
	              }else{
	              	return(0)
	              }
	            })
				
				const expenseArr = data.map(category => {
	          	  if(category.categoryType === 'expense'){
	          	  	return(
	          	  		category.amount
	          	  		)
	          	  }else{
	          	  	return(0)
	          	  }
	            })
	     
				setDateArray(dateArr)
				setIncomeArray(incomeArr)
				setExpenseArray(expenseArr)
			}
		})
	}, [])

	return(
			<React.Fragment>
				<Container>
				<LineChart dateArray = {dateArray} incomeArray = {incomeArray} expenseArray = {expenseArray}/>
				</Container>
			</React.Fragment>
		)
}