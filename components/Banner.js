import {useState, useContext} from 'react';
import {Form, Row, Col, Button} from 'react-bootstrap';
import UserContext from '../UserContext';
import Link from 'next/link';
import PropTypes from 'prop-types';
import Router from 'next/router';
import Swal from 'sweetalert2';

export default function Banner({dataProp}){
	const {title} = dataProp;

	const {token} = useContext(UserContext);

	function guest(){
		if(token === null){
            Swal.fire('Authentication Failed', 'You need to Login first. If you do not have an account, please register.', 'warning');
            Router.push('/login')
        }else{
            Router.push('/addRecord')
        	}
        }

	return(
			<Form>
				<Col className="align-left mt-4 pt-4">
			    	<h3>{title}</h3>
			    </Col>
			  <Form.Row>
			    <Col>
					<Button className="float-center ml-4" onClick={guest}>Add Record</Button>
			    </Col>
			  </Form.Row>
			</Form>
			)
}

Banner.propTypes = {
	data: PropTypes.shape({
		title: PropTypes.string.isRequired,
		content: PropTypes.string.isRequired,
		destination: PropTypes.string.isRequired,
		label1: PropTypes.string.isRequired,
		label2: PropTypes.string.isRequired,
		label3: PropTypes.string.isRequired
	})
}